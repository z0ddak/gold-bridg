export default { en: { message: {
      lang: 'English',
      hello: 'ZRELLO',
      search: 'Search',
      addcontact: 'Add contact',
      name: 'Name',
      company: 'Company',
      jobtitle: 'Activity',
      email: 'E-mal',
      notes: 'Notes',
      agreement: 'Agreement',
      cancel: 'Close',
      save: 'Save'

}}}
